# conambtel_cliente

## Descripción
Este proyecto permite la lectura de la placa de sensores conectada por Bluetooth en el computador
del teletrabajador, y el envio de notificaciones si las condiciones ambientales de cualquiera
de los sensores no se cumplen

## Requisitos
Para que este proyecto pueda funcionar se requiere que el computador a implementar se haya instalado lo siguiente:
Python en su version 3.11.6
git
repositorio central en MariaDB creado y configurado
parametros de conexión al repositorio central de MariaDB
accesos con un usuario y una contraseña al repositorio central de MariaDB que tenga permisos de lectura/escritura sobre la base de datos conambtel

## Instrucciones de Implementación

abrir la ventana de comandos de windows y en ella ubicarse con el comando cd en un directorio donde se pretende instalar el programa y seguir las sisguientes 
instrucciones:

clonar el repositorio
```
git clone https://gitlab.com/adomlugo/conambtel_cliente.git
cd conambtel_cliente
```

Instalar el ambiente virtual de python
```
python -m venv venv
```
Activar el ambiente virtual
```
cd venv\Scripts
activate
cd ..\..
```
Instalar los requerimientos del software
```
pip install -r requirements.txt
```
editar el archivo de configuración "configuración.yml" llenando cada parámetro de configuración de acuerdo a los requerimientos:
```
sensor:
  puerto: <specificar el puerto COM por donde se reciben los datos de la placa via bluetooth ejemplo puerto: COM11>
empresa:
  id: <colocar aqui el id de la empresa (debe existir en la base de datos). ejemplo id: 1>
base_de_datos:
  host: <colocar la url de conexión a la base de datos de MariaDB ejemplo host: localhost o host: xxxxxx.us-east-1.rds.amazonaws.com>
  port: <puerto de la instancia de MariaDB ejemplo port: 3306>
  database: conambtel
  user: <usuario con acceso lectura escritura a la base de datos de MariaDB ejemplo user: conan>
  pass: <clave del usuario anterior ejemplo pass: miclave>
teletrabajador:
  id: <colocar aqui el id del teletrabajador que será monitoriado (debe existir en la base de datos). ejemplo id: 1> 
```

ejecutar el programa
```
python main.py
```