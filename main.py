import serial
import sys
import os
import json
import datetime
import sqlite3
import yaml
import pymysql
import logging as log
from email.message import EmailMessage
import smtplib
from nicegui import native,app,run, ui
from plyer import notification

cache = sqlite3.connect("cache.db")

# constantes del programa
NOMBRE_FICHERO_CONFIGURACIÓN = "configuración.yml"

# ************************************************************************
# *** Funciones locales del programa no expuestas al frontend           **
# Leer archivo de configuracion local de la terminal del teletrabajador
def leer_configuración_local(ruta_fichero_configuración):
    contenido = ""
    with open(ruta_fichero_configuración,"r") as fichero_configuracion:
        contenido = yaml.safe_load(fichero_configuracion)
    return contenido

# crea conexión a la base de datos centralizada de Mariadb en la variable global conexion_remota
def obtener_conexión_remota():
    global configuración
    conexion_remota = None
    try:
        conexion_remota = pymysql.connect(
            host=configuración["base_de_datos"]["host"],
            port=configuración["base_de_datos"]["port"],
            user=configuración["base_de_datos"]["user"],
            passwd=configuración["base_de_datos"]["pass"],
            db=configuración["base_de_datos"]["database"],
            cursorclass=pymysql.cursors.DictCursor
        )
        conexion_remota.autocommit(True)
    except:
        log.error(f'conectando a base de datos remota, revise configuración local')
        sys.exit(1)
    return conexion_remota

# Leer el resto de configuraciones para la estación del teletrabajador en forma remota
# la cual se encuentra en la base de datos centralizada de MariaDb
def leer_configuración_remota():
    global configuración
    global conexion_remota
    with conexion_remota.cursor() as cursor:
        sql = """ 
            SELECT
                nit, 
                nombre,
                usuario, 
                host_correo_saliente, 
                usuario_correo_saliente, 
                contraseña_correo_saliente, 
                de_correo_saliente, 
                para_correo_saliente, 
                puerto_correo_saliente,
                luz_mínima,
                luz_máxima,
                ruido_mínimo,
                ruido_máximo,
                temperatura_mínima,
                temperatura_máxima,
                distancia_ideal,
                tiempo_pausa_activa,
                espera_entre_notificaciones,
                espera_entre_envíos_a_repositorio
            FROM 
                EMPRESA
            WHERE
                empresa_id = %s
        """
        cursor.execute(sql,(configuración["empresa"]["id"],))
        resultado = cursor.fetchone()
        configuración["empresa"]["configuracion_correo"]={}
        configuración["condiciones"]={}
        for clave in resultado:
            if clave in ["nit","nombre","usuario"]:
                configuración["empresa"][clave]=resultado[clave]
            elif clave in ["host_correo_saliente","usuario_correo_saliente","contraseña_correo_saliente","de_correo_saliente","para_correo_saliente","puerto_correo_saliente"]:
                configuración["empresa"]["configuracion_correo"][clave]=resultado[clave]
            elif clave in ["luz_mínima","luz_máxima","ruido_mínimo","ruido_máximo","temperatura_mínima","temperatura_máxima","distancia_ideal","tiempo_pausa_activa","espera_entre_notificaciones","espera_entre_envíos_a_repositorio"]:
                configuración["condiciones"][clave]=resultado[clave]

    with conexion_remota.cursor() as cursor:
        sql = """ 
            SELECT 
                cédula, 
                TELETRABAJADOR.nombre nombre,
                IF(género='M','Masculino','Femenino') género,
                fecha_nacimiento,
                fecha_ingreso,
                DEPARTAMENTO.nombre departamento,
                CIUDAD.nombre ciudad,
                correo
            FROM 
                TELETRABAJADOR,
                CIUDAD,
                DEPARTAMENTO
            WHERE
                TELETRABAJADOR.teletrabajador_id = %s AND 
                TELETRABAJADOR.ciudad_id = CIUDAD.ciudad_id AND 
                CIUDAD.departamento_id = DEPARTAMENTO.departamento_id
        """
        cursor.execute(sql,(configuración["teletrabajador"]["id"],))
        resultado = cursor.fetchone()
        for clave in resultado:
            configuración["teletrabajador"][clave]=resultado[clave]
                
# **************************************************************
# *** Declaración de Variables globales de la aplicacion ***
directorio_actual = None
configuración = None
conexion_remota = None
puerto = None
sensor = {}
chart1= None
chart2= None
chart3= None
chart4= None
chart5= None
tiempo= 0
anterior_notificacion_luz = None
anterior_notificacion_temperatura = None
anterior_notificacion_ruido = None
anterior_notificacion_tiempo = None
anterior_envio_repositorio = None
fecha_hora_anterior= None
menu_seleccionado ="color:#fff;border-left:solid 4px #3c8dbc"
menu_no_seleccionado ="color:#fff;border-left:solid 4px #222d32"

# procedimiento que crea la tabla mediciones si no existe
# dentro de una base de datos local sqlite 
# para usarla como cache de datos
def crear_cache():
    cursor = cache.cursor()
    sql = """
        CREATE TABLE IF NOT EXISTS MEDICIÓN (
            medición_id INTEGER PRIMARY KEY AUTOINCREMENT,
            fecha_hora TEXT,
            luz REAL,
            temperatura REAL,
            ruido REAL,
            pausa_activa INTEGER
        )
    """
    cursor.execute(sql)

# procedimiento para guardar de manera temporal
# las mediciones recibidas de los sensores de la estación 
# mientras llega el momento de enviarlas al repositorio centralizado de Mariadb
def guardar_en_cache():
    global sensor
    cursor = cache.cursor()
    sql = """
        INSERT INTO MEDICIÓN (
            fecha_hora,
            luz,
            temperatura,
            ruido,
            pausa_activa
        ) VALUES (
            ?,
            ?,
            ?,
            ?,
            ?
        )
    """
    cursor.execute(sql,(sensor['ahora'],sensor['luz'],sensor['temperatura'],sensor['ruido'],sensor['pausa_activa']))
    cursor.execute('COMMIT')

# procedimiento para leer la información temporal almacenada
# de los sensores en una base de datos local SQLite
# y luego enviarlos al repositorio centralizado de MariaDB
# para finalmente borrar los datos temporales almacenados localmente
def enviar_a_repositorio():
    global conexion_remota
    cursor = cache.cursor()
    sql = """
        SELECT 
            fecha_hora,
            luz,
            temperatura,
            ruido,
            pausa_activa
        FROM 
            MEDICIÓN
    """
    with conexion_remota.cursor() as cursor2:
        cursor.execute(sql)
        for fila in cursor:
                sql = """ 
                    INSERT INTO MEDICIÓN(
                        teletrabajador_id, 
                        fecha_hora, 
                        luz, 
                        temperatura, 
                        ruido,
                        pausa_activa)
                    VALUES (
                        %s,
                        %s,
                        %s,
                        %s,
                        %s,
                        %s
                    ) 
                """
                cursor2.execute(sql,(configuración["teletrabajador"]["id"],fila[0],fila[1],fila[2],fila[3],fila[4]))
        cursor2.execute("COMMIT")
    sql = """
        DELETE FROM MEDICIÓN
    """
    cursor.execute(sql)
    cursor.execute("commit")

# Punto de entrada para todas las páginas de la aplicación
@ui.page('/',favicon='pausa_activa.ico', title="ConAmbTel")
@ui.page('/{pagina}',favicon='pausa_activa.ico', title="ConAmbTel")
def page_layout(pagina="inicio"):
    ui.add_head_html('''
        <style>
            :root {
                --nicegui-default-padding: 16px;
                --nicegui-default-gap: 8px;
            }
        </style>
    ''')
    
    # creación del menú principal
    with ui.header(elevated=True).style('background-color: #3c8dbc' ).classes('items-center'):
        ui.button(on_click=lambda: left_drawer.toggle(), icon='menu').props('flat color=white')
        ui.label('CONDICIONES AMBIENTALES')
    with ui.left_drawer(top_corner=False, bottom_corner=False).style('background-color: #222d32; padding:0px 0px') as left_drawer:
        with ui.list().props('bordered separator').style("width: 100%"):
            ui.item_label('MENU PRINCIPAL').props('header').classes('text-bold').style("color:#fff")
            ui.separator()
            if pagina=="inicio":
                menu = menu_seleccionado
            else:
                menu = menu_no_seleccionado
            with ui.item(on_click=lambda: ui.open('inicio')).style(menu):
                with ui.item_section().props('avatar'):
                    ui.icon('person')
                with ui.item_section():
                    ui.item_label('Inicio')
            if pagina=="ver_configuración":
                menu = menu_seleccionado
            else:
                menu = menu_no_seleccionado
            with ui.item(on_click=lambda: ui.open('ver_configuración')).style(menu):
                with ui.item_section().props('avatar'):
                    ui.icon('person').style("color:#fff")
                with ui.item_section().style("color:#fff"):
                    ui.item_label('Ver configuración')

    # creación del pie de página
    with ui.footer().style('background-color: #333'):
        ui.label('ADL 2024')
    
    # contenido de la página de inicio (tablero principal con los medidores)
    if pagina=="inicio":
        with ui.card():
            ui.label('TABLERO DE CONTROL')
            ui.separator()
        with ui.row():
            with ui.card():
                ui.label('Temperatura en Grados Celsius')
                ui.separator()
                chart1 = ui.highchart({
                    'title': "Temperatura",
                    'pane': {
                        'startAngle': -90,
                        'endAngle': 89.9,
                        'background': None,
                        'center': ['50%', '75%'],
                        'size': '110%'
                    },
                    'chart': {'type': 'gauge'},
                    'series': [
                        {'data': [25],
                            'tooltip': { 'valueSuffix': ' Celsius' },
                            'animation': False
                        }
                    ],
                    'yAxis': {
                        'min': 15,
                        'max': 35,
                        "plotBands": [{
                            "from": 15,
                            "to": configuración["condiciones"]["temperatura_mínima"],
                            "color": '#DF5353', 
                            "thickness": 20
                        }, {
                            "from": configuración["condiciones"]["temperatura_mínima"],
                            "to": configuración["condiciones"]["temperatura_máxima"],
                            "color": '#55BF3B', 
                            "thickness": 20
                        }, {
                            "from": configuración["condiciones"]["temperatura_máxima"],
                            "to": 35,
                            "color": '#DF5353', 
                            "thickness": 20
                        }]
                    },
                }).classes('w-64 h-64')
                def actualizar_temperatura():
                    chart1.options['series'][0]['data'][0]=min(sensor["temperatura"],35) #app.storage.general.get('sensor')
                    chart1.update()
                ui.timer(1.0, lambda: actualizar_temperatura())
            with ui.card():
                ui.label('Luz ambiental (lux)')
                ui.separator()
                chart2 = ui.highchart({
                    'title': "Luz ambiental (lux)",
                    'pane': {
                        'startAngle': -90,
                        'endAngle': 89.9,
                        'background': None,
                        'center': ['50%', '75%'],
                        'size': '110%'
                    },
                    'chart': {'type': 'gauge'},
                    'series': [
                        {'data': [25],
                            'tooltip': { 'valueSuffix': ' Celsius' },
                            'animation': False
                        }
                    ],
                    'yAxis': {
                        'min': 0,
                        'max': 700,
                        "plotBands": [{
                            "from": 0,
                            "to": configuración["condiciones"]["luz_mínima"],
                            "color": '#DF5353', 
                            "thickness": 20
                        }, {
                            "from": configuración["condiciones"]["luz_mínima"],
                            "to": configuración["condiciones"]["luz_máxima"],
                            "color": '#55BF3B', 
                            "thickness": 20
                        }, {
                            "from": configuración["condiciones"]["luz_máxima"],
                            "to": 700,
                            "color": '#DF5353', 
                            "thickness": 20
                        }]
                    },
                }).classes('w-64 h-64')
                def actualizar_luz():
                    chart2.options['series'][0]['data'][0]=min(sensor["luz"],700) #app.storage.general.get('sensor')
                    chart2.update()
                ui.timer(1.0, lambda: actualizar_luz())
            with ui.card():
                ui.label('Ruido ambiental (db)')
                ui.separator()
                chart3 = ui.highchart({
                    'title': "Ruido ambiental (db)",
                    'pane': {
                        'startAngle': -90,
                        'endAngle': 89.9,
                        'background': None,
                        'center': ['50%', '75%'],
                        'size': '110%'
                    },
                    'chart': {'type': 'gauge'},
                    'series': [
                        {'data': [25],
                            'tooltip': { 'valueSuffix': ' Celsius' },
                            'animation': False
                        }
                    ],
                    'yAxis': {
                        'min': 0,
                        'max': 90,
                        "plotBands": [{
                            "from": 0,
                            "to": configuración["condiciones"]["ruido_máximo"],
                            "color": '#55BF3B', 
                            "thickness": 20
                        },{
                            "from": configuración["condiciones"]["ruido_máximo"],
                            "to": 90,
                            "color": '#DF5353', 
                            "thickness": 20
                        }]
                    },
                }).classes('w-64 h-64')
                def actualizar_ruido():
                    chart3.options['series'][0]['data'][0]=min(sensor["ruido"],90) #app.storage.general.get('sensor')
                    chart3.update()
                ui.timer(1.0, lambda: actualizar_ruido())
            with ui.card():
                ui.label('Distancia (cm)')
                ui.separator()
                chart4 = ui.highchart({
                    'title': "Distancia (cm)",
                    'pane': {
                        'startAngle': -90,
                        'endAngle': 89.9,
                        'background': None,
                        'center': ['50%', '75%'],
                        'size': '110%'
                    },
                    'chart': {'type': 'gauge'},
                    'series': [
                        {'data': [25],
                            'tooltip': { 'valueSuffix': ' Celsius' },
                            'animation': False
                        }
                    ],
                    'yAxis': {
                        'min': 0,
                        'max': 100,
                        "plotBands": [{
                            "from": 0,
                            "to": configuración["condiciones"]["distancia_ideal"],
                            "color": '#55BF3B', 
                            "thickness": 20
                        }, {
                            "from": configuración["condiciones"]["distancia_ideal"],
                            "to": 100,
                            "color":'#DDDF0D' , 
                            "thickness": 20
                        }]
                    },
                }).classes('w-64 h-64')
                def actualizar_distancia():
                    chart4.options['series'][0]['data'][0]=min(sensor["distancia"],100) #app.storage.general.get('sensor')
                    chart4.update()
                ui.timer(1.0, lambda: actualizar_distancia())
            with ui.card():
                ui.label('Tiempo para pausa activa (segundos)')
                ui.separator()
                chart5 = ui.highchart({
                    'title': "Tiempo para pausa activa (segundos)",
                    'pane': {
                        'startAngle': -90,
                        'endAngle': 89.9,
                        'background': None,
                        'center': ['50%', '75%'],
                        'size': '110%'
                    },
                    'chart': {'type': 'gauge'},
                    'series': [
                        {'data': [0],
                            'tooltip': { 'valueSuffix': ' Segundos' },
                            'animation': False
                        }
                    ],
                    'yAxis': {
                        'min': 0,
                        'max': configuración["condiciones"]["tiempo_pausa_activa"] *60*60,
                        "plotBands": [{
                            "from": 0,
                            "to": configuración["condiciones"]["tiempo_pausa_activa"] *60*60,
                            "color": '#55BF3B', 
                            "thickness": 20
                        }]
                    },
                }).classes('w-64 h-64')

                def actualizar_tiempo():
                    global tiempo
                    tiempo= tiempo + 1
                    if sensor["distancia"] > configuración["condiciones"]["distancia_ideal"]:
                        tiempo = 0
                    chart5.options['series'][0]['data'][0]=min(tiempo,configuración["condiciones"]["tiempo_pausa_activa"] *60*60)
                    chart5.update()
                    
                ui.timer(1.0, lambda: actualizar_tiempo())
        with ui.card().classes('w-full'):
            ui.label('Datos')
            grid = ui.aggrid({
                'defaultColDef': {'flex': 1},
                'columnDefs': [
                    {'headerName': 'Fecha y hora', 'field': 'fecha_hora'},
                    {'headerName': 'Temperatura (celsius)', 'field': 'temperatura'},
                    {'headerName': 'Luz (lux)', 'field': 'luz'},
                    {'headerName': 'Ruido (db)', 'field': 'ruido'},
                    {'headerName': 'Distancia (cm)', 'field': 'distancia'},
                ],
                'rowData': [
                    {'fecha_hora': None, 'temperatura': None, 'luz': None,'ruido':None,'distancia':None},
                    {'fecha_hora': None, 'temperatura': None, 'luz': None,'ruido':None,'distancia':None},
                    {'fecha_hora': None, 'temperatura': None, 'luz': None,'ruido':None,'distancia':None},
                    {'fecha_hora': None, 'temperatura': None, 'luz': None,'ruido':None,'distancia':None},
                    {'fecha_hora': None, 'temperatura': None, 'luz': None,'ruido':None,'distancia':None},
                    {'fecha_hora': None, 'temperatura': None, 'luz': None,'ruido':None,'distancia':None},
                    {'fecha_hora': None, 'temperatura': None, 'luz': None,'ruido':None,'distancia':None},
                ],
                'rowSelection': 'multiple',
            }).classes('w-full h-64')
            def actualizar_grilla():
                global fecha_hora_anterior
                if fecha_hora_anterior != sensor["ahora"]:
                    for i in range(6):
                        grid.options['rowData'][6-i] =  grid.options['rowData'][6-i-1]  
        
                    grid.options['rowData'][0]={
                            "fecha_hora":sensor["ahora"],
                            "temperatura": sensor["temperatura"],
                            "luz":sensor["luz"],
                            "ruido":sensor["ruido"],
                            "distancia":sensor["distancia"]
                    }
                    grid.update()
                    fecha_hora_anterior = sensor["ahora"]
            global fecha_hora_anterior
            fecha_hora_anterior=None
            ui.timer(1.0, lambda: actualizar_grilla())
    
    # contenido de la página ver configuraciones
    # permite visualizar los parámetros para la estación
    elif pagina=="ver_configuración":
        with ui.card().classes('w-1/2'):
            ui.label('DATOS DE LA COMPAÑIA').classes('font-bold')
            ui.separator()
            with ui.grid(columns=2):
                ui.label('NIT:')
                ui.label(configuración["empresa"]["nit"])
                ui.label('Razón social:')
                ui.label(configuración["empresa"]["nombre"])
        with ui.card().classes('w-1/2'):
            ui.label('DATOS DEL TELETRABAJADOR').classes('font-bold')
            ui.separator()
            with ui.grid(columns=2):
                ui.label('Identificación:')
                ui.label(configuración["teletrabajador"]["cédula"])
                ui.label('Nombre:')
                ui.label(configuración["teletrabajador"]["nombre"])
                ui.label('Género:')
                ui.label(configuración["teletrabajador"]["género"])
                ui.label('Fecha de nacimiento:')
                ui.label(configuración["teletrabajador"]["fecha_nacimiento"])
                ui.label('Fecha de ingreso:')
                ui.label(configuración["teletrabajador"]["fecha_ingreso"])
                ui.label('Departamento:')
                ui.label(configuración["teletrabajador"]["departamento"])
                ui.label('Ciudad:')
                ui.label(configuración["teletrabajador"]["ciudad"])
        with ui.card().classes('w-1/2'):
            ui.label('INTERVALO IDEAL DE MEDICIONES').classes('font-bold')
            ui.separator()
            columnas = [
                {'name': 'sensor', 'label': 'Sensor', 'field': 'sensor', 'required': True, 'align': 'left'},
                {'name': 'desde', 'label': 'Desde', 'field': 'desde', 'required': True, 'align': 'right'},
                {'name': 'hasta', 'label': 'Hasta', 'field': 'hasta', 'required': True, 'align': 'right'},
            ]
            filas = [
                {'sensor': 'Temperatura (C)', 'desde': configuración["condiciones"]["temperatura_mínima"], 'hasta':configuración["condiciones"]["temperatura_máxima"]},
                {'sensor': 'Luz ambiental (lux)', 'desde': configuración["condiciones"]["luz_mínima"], 'hasta':configuración["condiciones"]["luz_máxima"]},
                {'sensor': 'Ruido ambiental (db)', 'desde': configuración["condiciones"]["ruido_mínimo"], 'hasta':configuración["condiciones"]["ruido_máximo"]},
                {'sensor': 'Distancia (cm)', 'desde': 0, 'hasta':configuración["condiciones"]["distancia_ideal"]},
            ]
            ui.table(columns=columnas, rows=filas, row_key='sensor')

# procedimiento que permite enviar correo con los parametros
# del servidor de correos de la empresa
def send_email(email_to, subject, message):
    try:
        email = EmailMessage()
        email["From"] = configuración["empresa"]["configuracion_correo"]["de_correo_saliente"]
        email["To"] = email_to
        email["Subject"] = subject
        email.set_content(message, subtype="html")
        smtp = smtplib.SMTP_SSL(configuración["empresa"]["configuracion_correo"]["host_correo_saliente"])
        smtp.default_port=int(configuración["empresa"]["configuracion_correo"]["puerto_correo_saliente"])
        smtp.login(configuración["empresa"]["configuracion_correo"]["usuario_correo_saliente"], configuración["empresa"]["configuracion_correo"]["contraseña_correo_saliente"])
        smtp.sendmail(configuración["empresa"]["configuracion_correo"]["de_correo_saliente"], email_to, email.as_string())
        smtp.quit()
    except:
        log.error(f'Sending email')            

# procedimiento para leer la configuracion local almacenada 
# en un fichero local llamado configuración.yml
def leer_configuracion():
    global directorio_actual
    global configuración
    global conexion_remota
    directorio_actual = os.getcwd()
    configuración = leer_configuración_local(os.path.join(directorio_actual,NOMBRE_FICHERO_CONFIGURACIÓN))
    conexion_remota = obtener_conexión_remota()
    leer_configuración_remota()

# procedimiento para comprobar y enviar notificaciones y correos a los usuarios
# en caso de que alguno de los sensores no cumpla con los parámetros ideales
def comprobar_notificaciones():
    global sensor
    global anterior_notificacion_luz
    global anterior_notificacion_temperatura
    global anterior_notificacion_ruido
    global anterior_notificacion_tiempo
    mensaje_cab = ""
    mensaje_cab = mensaje_cab + "<b>Condiciones ambientales no ideales</b><br><br>"
    mensaje_cab = mensaje_cab + "<table>"
    mensaje_cab = mensaje_cab + "<tr>"
    mensaje_cab = mensaje_cab + "   <td colspan=2><b>Datos de la empresa</b></td>"
    mensaje_cab = mensaje_cab + "</tr>"
    mensaje_cab = mensaje_cab + "<tr>"
    mensaje_cab = mensaje_cab + "   <td>NIT:</td>"
    mensaje_cab = mensaje_cab + "   <td>"+ configuración["empresa"]["nit"] +"</td>"
    mensaje_cab = mensaje_cab + "</tr>"
    mensaje_cab = mensaje_cab + "<tr>"
    mensaje_cab = mensaje_cab + "   <td>Empresa:</td>"
    mensaje_cab = mensaje_cab + "   <td>"+ configuración["empresa"]["nombre"] +"<br></td>"
    mensaje_cab = mensaje_cab + "</tr>"
    mensaje_cab = mensaje_cab + "<tr><td colspan=2></td></tr>"
    mensaje_cab = mensaje_cab + "<tr>"
    mensaje_cab = mensaje_cab + "   <td colspan=2><b>Datos del teletrabajador</b></td>"
    mensaje_cab = mensaje_cab + "</tr>"
    mensaje_cab = mensaje_cab + "<tr>"
    mensaje_cab = mensaje_cab + "   <td>Cédula:</td>"
    mensaje_cab = mensaje_cab + "   <td>"+ configuración["teletrabajador"]["cédula"] +"</td>"
    mensaje_cab = mensaje_cab + "</tr>"
    mensaje_cab = mensaje_cab + "<tr>"
    mensaje_cab = mensaje_cab + "   <td>Nombre del Teletrabajador:</td>"
    mensaje_cab = mensaje_cab + "   <td>"+ configuración["teletrabajador"]["nombre"] +"<br></td>"
    mensaje_cab = mensaje_cab + "</tr>"
    mensaje_cab = mensaje_cab + "<tr><td colspan=2></td></tr>"
    mensaje_cab = mensaje_cab + "<tr>"
    mensaje_cab = mensaje_cab + "   <td colspan=2><b>Datos de la alarma</b></td>"
    mensaje_cab = mensaje_cab + "</tr>"

    if float(sensor["temperatura"]) < float(configuración["condiciones"]["temperatura_mínima"]) and ((datetime.datetime.now() - anterior_notificacion_temperatura).total_seconds() >= configuración["condiciones"]["espera_entre_notificaciones"]) and (sensor["distancia"] <= configuración["condiciones"]["distancia_ideal"]):
        notification.notify(
            title="Condiciones ambientales no ideales", 
            app_name="", 
            message=f"La temperatura es baja en su ambiente de trabajo: {sensor['temperatura']} Celsius",
            app_icon="temperatura.ico")
        anterior_notificacion_temperatura = datetime.datetime.now()
        mensaje = mensaje_cab
        mensaje = mensaje + "<tr>"
        mensaje = mensaje + "   <td>Fecha y hora:</td>"
        mensaje = mensaje + "   <td>"+ datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') +"</td>"
        mensaje = mensaje + "</tr>"
        mensaje = mensaje + "<tr>"
        mensaje = mensaje + "   <td>Sensor</td>"
        mensaje = mensaje + "   <td>temperatura</td>"
        mensaje = mensaje + "</tr>"
        mensaje = mensaje + "<tr>"
        mensaje = mensaje + "   <td>Unidad de medida:</td>"
        mensaje = mensaje + "   <td>Celsius</td>"
        mensaje = mensaje + "</tr>"
        mensaje = mensaje + "<tr>"
        mensaje = mensaje + "   <td>Rango ideal:</td>"
        mensaje = mensaje + "   <td>desde: "+ str(configuración["condiciones"]["temperatura_mínima"]) +" grados celsius   hasta: "+str(configuración["condiciones"]["temperatura_máxima"]) +" grados celsius</td>"
        mensaje = mensaje + "</tr>"
        mensaje = mensaje + "<tr>"
        mensaje = mensaje + "   <td>Lectura:</td>"
        mensaje = mensaje + "   <td>"+ str(sensor["temperatura"]) +" grados celsius</td>"
        mensaje = mensaje + "</tr>"
        mensaje = mensaje + "<tr>"
        mensaje = mensaje + "   <td>Observación:</td>"
        mensaje = mensaje + "   <td>temperatura baja en el ambiente de trabajo</td>"
        mensaje = mensaje + "</tr>"
        mensaje = mensaje + "</table>"
        send_email(configuración["teletrabajador"]["correo"],"Alerta de condiciones ambientales de trabajo",mensaje)
        send_email(configuración["empresa"]["usuario"],"Alerta de condiciones ambientales de trabajo",mensaje)

    
    if float(sensor["temperatura"]) > float(configuración["condiciones"]["temperatura_máxima"]) and ((datetime.datetime.now() - anterior_notificacion_temperatura).total_seconds() >= configuración["condiciones"]["espera_entre_notificaciones"]) and (sensor["distancia"] <= configuración["condiciones"]["distancia_ideal"]):
        notification.notify(
            title="Condiciones ambientales no ideales", 
            app_name="", 
            message=f"La temperatura esta alta en su ambiente de trabajo: {sensor['temperatura']} Celsius",
            app_icon="temperatura.ico")
        anterior_notificacion_temperatura = datetime.datetime.now()
        mensaje = mensaje_cab
        mensaje = mensaje + "<tr>"
        mensaje = mensaje + "   <td>Fecha y hora:</td>"
        mensaje = mensaje + "   <td>"+ datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') +"</td>"
        mensaje = mensaje + "</tr>"
        mensaje = mensaje + "<tr>"
        mensaje = mensaje + "   <td>Sensor</td>"
        mensaje = mensaje + "   <td>temperatura</td>"
        mensaje = mensaje + "</tr>"
        mensaje = mensaje + "<tr>"
        mensaje = mensaje + "   <td>Unidad de medida:</td>"
        mensaje = mensaje + "   <td>Celsius</td>"
        mensaje = mensaje + "</tr>"
        mensaje = mensaje + "<tr>"
        mensaje = mensaje + "   <td>Rango ideal:</td>"
        mensaje = mensaje + "   <td>desde: "+ str(configuración["condiciones"]["temperatura_mínima"]) +" grados celsius   hasta: "+str(configuración["condiciones"]["temperatura_máxima"]) +" grados celsius</td>"
        mensaje = mensaje + "</tr>"
        mensaje = mensaje + "<tr>"
        mensaje = mensaje + "   <td>Lectura:</td>"
        mensaje = mensaje + "   <td>"+ str(sensor["temperatura"]) +" grados celsius</td>"
        mensaje = mensaje + "</tr>"
        mensaje = mensaje + "<tr>"
        mensaje = mensaje + "   <td>Observación:</td>"
        mensaje = mensaje + "   <td>temperatura alta en el ambiente de trabajo</td>"
        mensaje = mensaje + "</tr>"
        mensaje = mensaje + "</table>"
        send_email(configuración["teletrabajador"]["correo"],"Alerta de condiciones ambientales de trabajo",mensaje)
        send_email(configuración["empresa"]["usuario"],"Alerta de condiciones ambientales de trabajo",mensaje)

    if float(sensor["luz"]) < float(configuración["condiciones"]["luz_mínima"]) and ((datetime.datetime.now() - anterior_notificacion_luz).total_seconds() >= configuración["condiciones"]["espera_entre_notificaciones"]) and (sensor["distancia"] <= configuración["condiciones"]["distancia_ideal"]):
        notification.notify(
            title="Condiciones ambientales no ideales", 
            app_name="", 
            message=f"Hay poca luz en su estación de trabajo: {sensor['luz']} lux",
            app_icon="luz.ico")
        anterior_notificacion_luz = datetime.datetime.now()
        mensaje = mensaje_cab
        mensaje = mensaje + "<tr>"
        mensaje = mensaje + "   <td>Fecha y hora:</td>"
        mensaje = mensaje + "   <td>"+ datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') +"</td>"
        mensaje = mensaje + "</tr>"
        mensaje = mensaje + "<tr>"
        mensaje = mensaje + "   <td>Sensor</td>"
        mensaje = mensaje + "   <td>luz</td>"
        mensaje = mensaje + "</tr>"
        mensaje = mensaje + "<tr>"
        mensaje = mensaje + "   <td>Unidad de medida:</td>"
        mensaje = mensaje + "   <td>lux</td>"
        mensaje = mensaje + "</tr>"
        mensaje = mensaje + "<tr>"
        mensaje = mensaje + "   <td>Rango ideal:</td>"
        mensaje = mensaje + "   <td>desde: "+ str(configuración["condiciones"]["luz_mínima"]) +" lux   hasta: "+str(configuración["condiciones"]["luz_máxima"]) +" lux</td>"
        mensaje = mensaje + "</tr>"
        mensaje = mensaje + "<tr>"
        mensaje = mensaje + "   <td>Lectura:</td>"
        mensaje = mensaje + "   <td>"+ str(sensor["luz"]) +" lux</td>"
        mensaje = mensaje + "</tr>"
        mensaje = mensaje + "<tr>"
        mensaje = mensaje + "   <td>Observación:</td>"
        mensaje = mensaje + "   <td>Hay poca luz en la estación de trabajo</td>"
        mensaje = mensaje + "</tr>"
        mensaje = mensaje + "</table>"
        send_email(configuración["teletrabajador"]["correo"],"Alerta de condiciones ambientales de trabajo",mensaje)
        send_email(configuración["empresa"]["usuario"],"Alerta de condiciones ambientales de trabajo",mensaje)

    if float(sensor["luz"]) > float(configuración["condiciones"]["luz_máxima"]) and ((datetime.datetime.now() - anterior_notificacion_luz).total_seconds() >= configuración["condiciones"]["espera_entre_notificaciones"]) and (sensor["distancia"] <= configuración["condiciones"]["distancia_ideal"]):
        notification.notify(
            title="Condiciones ambientales no ideales", 
            app_name="", 
            message=f"Hay demasiada luz en su estación de trabajo: {sensor['luz']} lux",
            app_icon="luz.ico")
        anterior_notificacion_luz = datetime.datetime.now()
        mensaje = mensaje_cab
        mensaje = mensaje + "<tr>"
        mensaje = mensaje + "   <td>Fecha y hora:</td>"
        mensaje = mensaje + "   <td>"+ datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') +"</td>"
        mensaje = mensaje + "</tr>"
        mensaje = mensaje + "<tr>"
        mensaje = mensaje + "   <td>Sensor</td>"
        mensaje = mensaje + "   <td>luz</td>"
        mensaje = mensaje + "</tr>"
        mensaje = mensaje + "<tr>"
        mensaje = mensaje + "   <td>Unidad de medida:</td>"
        mensaje = mensaje + "   <td>lux</td>"
        mensaje = mensaje + "</tr>"
        mensaje = mensaje + "<tr>"
        mensaje = mensaje + "   <td>Rango ideal:</td>"
        mensaje = mensaje + "   <td>desde: "+ str(configuración["condiciones"]["luz_mínima"]) +" lux   hasta: "+str(configuración["condiciones"]["luz_máxima"]) +" lux</td>"
        mensaje = mensaje + "</tr>"
        mensaje = mensaje + "<tr>"
        mensaje = mensaje + "   <td>Lectura:</td>"
        mensaje = mensaje + "   <td>"+ str(sensor["luz"]) +" lux</td>"
        mensaje = mensaje + "</tr>"
        mensaje = mensaje + "<tr>"
        mensaje = mensaje + "   <td>Observación:</td>"
        mensaje = mensaje + "   <td>Demasiada luz en su estación de trabajo</td>"
        mensaje = mensaje + "</tr>"
        mensaje = mensaje + "</table>"
        send_email(configuración["teletrabajador"]["correo"],"Alerta de condiciones ambientales de trabajo",mensaje)
        send_email(configuración["empresa"]["usuario"],"Alerta de condiciones ambientales de trabajo",mensaje)

    if float(sensor["ruido"]) > float(configuración["condiciones"]["ruido_máximo"]) and ((datetime.datetime.now() - anterior_notificacion_ruido).total_seconds() >= configuración["condiciones"]["espera_entre_notificaciones"]) and (sensor["distancia"] <= configuración["condiciones"]["distancia_ideal"]):
        notification.notify(
            title="Condiciones ambientales no ideales", 
            app_name="", 
            message=f"Hay mucho ruido en su estación de trabajo: {sensor['ruido']} decibeles",
            app_icon="ruido.ico")
        anterior_notificacion_ruido = datetime.datetime.now()
        anterior_notificacion_luz = datetime.datetime.now()
        mensaje = mensaje_cab
        mensaje = mensaje + "<tr>"
        mensaje = mensaje + "   <td>Fecha y hora:</td>"
        mensaje = mensaje + "   <td>"+ datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') +"</td>"
        mensaje = mensaje + "</tr>"
        mensaje = mensaje + "<tr>"
        mensaje = mensaje + "   <td>Sensor</td>"
        mensaje = mensaje + "   <td>ruido</td>"
        mensaje = mensaje + "</tr>"
        mensaje = mensaje + "<tr>"
        mensaje = mensaje + "   <td>Unidad de medida:</td>"
        mensaje = mensaje + "   <td>Decibel</td>"
        mensaje = mensaje + "</tr>"
        mensaje = mensaje + "<tr>"
        mensaje = mensaje + "   <td>Rango ideal:</td>"
        mensaje = mensaje + "   <td>desde: "+ str(configuración["condiciones"]["ruido_mínimo"]) +" db   hasta: "+str(configuración["condiciones"]["ruido_máximo"]) +" db</td>"
        mensaje = mensaje + "</tr>"
        mensaje = mensaje + "<tr>"
        mensaje = mensaje + "   <td>Lectura:</td>"
        mensaje = mensaje + "   <td>"+ str(sensor["ruido"]) +" db</td>"
        mensaje = mensaje + "</tr>"
        mensaje = mensaje + "<tr>"
        mensaje = mensaje + "   <td>Observación:</td>"
        mensaje = mensaje + "   <td>Hay mucho ruido en en el ambiente de trabajo</td>"
        mensaje = mensaje + "</tr>"
        mensaje = mensaje + "</table>"
        send_email(configuración["teletrabajador"]["correo"],"Alerta de condiciones ambientales de trabajo",mensaje)
        send_email(configuración["empresa"]["usuario"],"Alerta de condiciones ambientales de trabajo",mensaje)

    if (tiempo > configuración["condiciones"]["tiempo_pausa_activa"] *60*60) and ((datetime.datetime.now() - anterior_notificacion_tiempo).total_seconds() >= configuración["condiciones"]["espera_entre_notificaciones"]):
        notification.notify(
            title="Condiciones ambientales no ideales", 
            app_name="", 
            message=f"Ha llegado el momento de hacer una pausa activa",
            app_icon="pausa_activa.ico")
        anterior_notificacion_tiempo = datetime.datetime.now()

# procedimiento que funciona cíclica y permite leer los datos enviados por los sensores conectados de manera local
# por un puerto COM usando Bluetooth 
# 1. comprueba si hay que enviar notificaciones
# 2. almacena en cache los datos recibidos en una base de datos SQlite
# 3. envia los datos al repositorio central en MariaDB cada cierto tiempo.
async def leer_datos():
    global sensor
    global anterior_notificacion_luz
    global anterior_notificacion_temperatura
    global anterior_notificacion_ruido
    global anterior_notificacion_tiempo
    global anterior_envio_repositorio
    global configuración
    global tiempo
    crear_cache()
    leer_configuracion()
    puerto = serial.Serial(configuración["sensor"]["puerto"], baudrate=9600, timeout=0)
    anterior_notificacion_luz = datetime.datetime.now() - datetime.timedelta(seconds=configuración["condiciones"]["espera_entre_notificaciones"])
    anterior_notificacion_temperatura= datetime.datetime.now() - datetime.timedelta(seconds=configuración["condiciones"]["espera_entre_notificaciones"])
    anterior_notificacion_ruido= datetime.datetime.now() - datetime.timedelta(seconds=configuración["condiciones"]["espera_entre_notificaciones"])
    anterior_notificacion_tiempo= datetime.datetime.now() - datetime.timedelta(seconds=configuración["condiciones"]["espera_entre_notificaciones"])
    anterior_envio_repositorio= datetime.datetime.now()
    while not app.is_stopped:
        dato = await run.io_bound(puerto.readline)
        if dato:
            try:
                sensor = json.loads(dato)
                sensor["ahora"]= datetime.datetime.strftime(datetime.datetime.now(), "%Y-%m-%d %H:%M:%S")
                sensor["distancia"]=round(sensor["distancia"]/10,0)
                if tiempo > configuración["condiciones"]["tiempo_pausa_activa"] *60*60:
                    sensor["pausa_activa"]=1
                else:
                    sensor["pausa_activa"]=0
                comprobar_notificaciones()
                if ((datetime.datetime.now() - anterior_envio_repositorio).total_seconds() < configuración["condiciones"]["espera_entre_envíos_a_repositorio"]):
                    guardar_en_cache()
                else:
                    enviar_a_repositorio()
                    anterior_envio_repositorio = datetime.datetime.now()
            except:
                print("error de lectura valor del dato crudo:")
                print(dato)
app.on_startup(leer_datos)

ui.run(port=8081)
